#!/usr/bin/env bash

set -o xtrace

export DEBIAN_FRONTEND=noninteractive

export REPO="https://gitlab.com/thomas_kriechbaumer/server-init/raw/master"

if [[ "$EUID" -ne 0 ]]; then
  if [[ -n "$(command -v sudo)" ]]; then
    PREFIX="sudo "
    echo "Not running as root, using sudo instead."
  else
    echo "Not running as root, and no sudo found. Exiting."
    exit 1
  fi
else
  PREFIX=""
  echo "Running as root - no sudo needed for individual commands."
fi

if [[ -n "$(command -v yum)" ]]; then
  ${PREFIX} yum check-update
  INSTALLER="yum install -y"
elif [[ -n "$(command -v apt-get)" ]]; then
  ${PREFIX} apt-get update
  INSTALLER="apt-get -y install"
fi

${PREFIX} ${INSTALLER} \
  curl \
  git \
  grep \
  htop \
  mosh \
  ncdu \
  nmon \
  locales \
  sudo \
  tmux \
  tree \
  vim \
  wget \
  zip \
  zsh

echo 'LANG="en_US.UTF-8"' | ${PREFIX} tee /etc/default/locale > /dev/null
${PREFIX} sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
${PREFIX} locale-gen "en_US.UTF-8"
${PREFIX} dpkg-reconfigure --frontend=noninteractive locales
${PREFIX} update-locale LANG=en_US.UTF-8

curl https://beyondgrep.com/ack-v3.7.0 | ${PREFIX} tee /bin/ack > /dev/null
${PREFIX} chmod 0755 /bin/ack

curl --proto '=https' --tlsv1.2 -LsSf https://setup.atuin.sh | sh
mkdir -p ~/.config/atuin/
curl -fsSL $REPO/configs/atuin.config.toml > ~/.config/atuin/config.toml
(
  source $HOME/.atuin/bin/env
  atuin import bash
  atuin import zsh
)

SH_NAME_ESCAPED=$(grep '/zsh$' /etc/shells | head -1 | sed -r 's|/|\\/|g')

${PREFIX} sed -i -r "/^$(whoami)/s/:\/[^:]+?$/:$SH_NAME_ESCAPED/" /etc/passwd
${PREFIX} chsh -s $(grep /zsh$ /etc/shells | head -1) $(whoami)

git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh

mkdir -p ~/.config/htop
echo "hide_kernel_threads=1" >> ~/.config/htop/htoprc
echo "hide_userland_threads=1" >> ~/.config/htop/htoprc
echo "tree_view=1" >> ~/.config/htop/htoprc

curl -fsSL $REPO/configs/tmux.conf > ~/.tmux.conf
curl -fsSL $REPO/configs/vimrc > ~/.vimrc
curl -fsSL $REPO/configs/zshrc > ~/.zshrc

if [[ "$EUID" -ne 0 ]]; then
  sudo bash -c "sed -i -r \"/^root/s/:\/[^:]+?$/:$SH_NAME_ESCAPED/\" /etc/passwd"
  sudo chsh -s $(grep /zsh$ /etc/shells | head -1) root

  sudo bash -c 'git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh'

  sudo bash -c 'mkdir -p ~/.config/htop'
  sudo bash -c 'echo "hide_kernel_threads=1" >> ~/.config/htop/htoprc'
  sudo bash -c 'echo "hide_userland_threads=1" >> ~/.config/htop/htoprc'
  sudo bash -c 'echo "tree_view=1" >> ~/.config/htop/htoprc'

  sudo bash -c "curl -fsSL $REPO/configs/tmux.conf > ~/.tmux.conf"
  sudo bash -c "curl -fsSL $REPO/configs/vimrc > ~/.vimrc"
  sudo bash -c "curl -fsSL $REPO/configs/zshrc > ~/.zshrc"

  sudo bash -c "mkdir -p ~/.config/atuin/"
  sudo bash -c "curl -fsSL $REPO/configs/atuin.config.toml > ~/.config/atuin/config.toml"
  sudo bash -c "source $HOME/.atuin/bin/env && atuin import bash"
  sudo bash -c "source $HOME/.atuin/bin/env && atuin import zsh"
fi
