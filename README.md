# server-init

Collection of scripts and config files for setting up new servers.

Run this command:
```sh
sh -c "$(wget https://gitlab.com/thomas_kriechbaumer/server-init/raw/master/init.sh -O -)"

Or:
```sh
curl -L https://gitlab.com/thomas_kriechbaumer/server-init/raw/master/init.sh | sh
```
